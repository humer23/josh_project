'use strict'

//Click function
$(document).ready(function() {
    // open and close burger menu on click
    $('.burger_menu').click(function() {
        $('.burger_menu').toggleClass('open-menu');
        $('.nav_menu').toggleClass('open-menu');
    });
    // open form for download resume on click button
    $('.button').click(function() {
        $('.download_form').toggleClass('open_download_section');
    });
    // close form for download resume on click close form
    $('.close_form_cross').click(function() {
        $('.download_form').removeClass('open_download_section');
    });
});

//Slider init
$(document).ready(function(){
    $('.posts_content').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
    });
});